import 'package:flutter/material.dart';

class ChatMessage extends StatelessWidget {
  ChatMessage(this._data, this._mine);

  final Map<String, dynamic> _data;
  final bool _mine;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        children: <Widget>[
          !_mine
              ? Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(_data["senderPhotoUrl"]),
                  ),
                )
              : Container(),
          Expanded(
            child: Column(
              crossAxisAlignment:
                  !_mine ? CrossAxisAlignment.start : CrossAxisAlignment.end,
              children: <Widget>[
                _data["imgUrl"] != null
                    ? Image.network(
                        _data["imgUrl"],
                        width: 250,
                      )
                    : Text(
                        _data["text"] ?? "",
                        textAlign: _mine ? TextAlign.end : TextAlign.start,
                        style: TextStyle(fontSize: 16),
                      ),
                Text(_data["senderName"],
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500))
              ],
            ),
          ),
          _mine
              ? Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(_data["senderPhotoUrl"]),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
